var dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";	//Character dictionary for code table

//Vigenere encryption function TODO: chnage name to vigenereEn()
function enCrypt() {
	var key = document.getElementById("key").value.replace(/\s+/g,'').toUpperCase();	//Read key field and delete all spaces
	var text = document.getElementById("plaintext").value.toUpperCase();		//Read plantext field
	var crypText = "";
	if(key != "" && text != "") {
		var textIndex;	//Iterator for plaintext
		var keyIndex = 0;	//Iterator for key
		var place;			//Place of plaintext character in dictionary
		for(textIndex = 0; textIndex < text.length; ++textIndex) {
			//Ignor spaces
			if(text.charAt(textIndex) != " ") {
				place = dictionary.indexOf(text.charAt(textIndex));	//Get the place of the given paintext character in the dictionary
				cryptCharIndex = dictionary.indexOf(key.charAt(keyIndex)) + place;		//The position of the encryption character in the dictionary
				if(cryptCharIndex >= dictionary.length) {
					//If the position if greater than the length of the dictionary start from the begining
					cryptCharIndex = cryptCharIndex - dictionary.length;
				}
				crypText += dictionary.charAt(cryptCharIndex);
				//Incrementing the keyIndex and if it equals the length of the key reset to 0
				if(++keyIndex >= key.length) {
					keyIndex = 0;
				}
			}
		}
	}
	document.getElementById("encrypt").innerHTML = crypText;
}

function deCrypt() {
	var key = document.getElementById("key").value.replace(/\s+/g,'').toUpperCase();	//Read key field and delete all spaces
	var text = document.getElementById("cryptext").value.toUpperCase();		//Read plantext field
	var plainText = "";
	if(key != "" && text != "") {
		var textIndex;
		var keyIndex = 0;
		var place;
		for(textIndex = 0; textIndex < text.length; ++textIndex) {
			if(dictionary.indexOf(text.charAt(textIndex)) >= dictionary.indexOf(key.charAt(keyIndex))) {
				place = dictionary.indexOf(text.charAt(textIndex)) - dictionary.indexOf(key.charAt(keyIndex));
			}
			else {
				place = dictionary.length - (dictionary.indexOf(key.charAt(keyIndex)) - dictionary.indexOf(text.charAt(textIndex)));
			}
			plainText += dictionary.charAt(place);
			if(++keyIndex >= key.length) {
					keyIndex = 0;
				}
		}
	}
	document.getElementById("decrypt").innerHTML = plainText;
}